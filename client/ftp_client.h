#if !defined(FTP_CLIENT)
#define FTP_CLIENT
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include "menu.h"
#include "../general/display_progress_bar.h"
#define SIZE 1024
#define MAX_USERNAME_LENGTH 50
#define MAX_PASSWORD_LENGTH 50
#define MAX_DATA_LENGTH 300
#define FILENAME_LENGTH 300
#define IP "127.0.0.1"
#define PORT 8088
int send_file(char * file_name,int sockfd);
int create_socket();
int _download_file(char *file_name, int sockfd);
int login(int sockfd);
void toUpperCase(char *ch);
#endif // FTP_CLIENT

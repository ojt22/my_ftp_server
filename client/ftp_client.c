#include "ftp_client.h"
// gcc -o ftp_cli ftp_client.c

int main()
{
  char filename[FILENAME_LENGTH] = {0};
  int sockfd;
  sockfd = create_socket();

  while (login(sockfd) == 0)
  {
    printf("\nlogin failed!\n");
    close(sockfd);
    sockfd = create_socket();
  }

  close(sockfd);
  print_menu();
  printf("[+] Closing the connection.\n");
  close(sockfd);

  return 0;
}

int _download_file(char *file_name, int sockfd)
{
  char buffer[SIZE] = {0};
  size_t bytesRead;
  char data[] = "download";
  printf("Sending login data: %s\n", data);
  sprintf(data, "download %s", file_name);
  if (send(sockfd, data, strlen(data), 0) == -1)
  {
    perror("[-]Error in sending login data.");
    return 0;
  }
  char file_name2[SIZE] = {0};
  int n;
  FILE *fp;
  *buffer = '\0';
  sprintf(file_name2, "download_%s", file_name);
  fp = fopen(file_name2, "w");

  char *ptr = NULL;
  int i = 0;
  // do
  // {
  //   n = recv(sockfd, buffer, SIZE, 0);
  //   ptr = buffer;
  //   printf("\n%s", ptr);

  //   fprintf(fp, "%d. %s", ++i, ptr);
  //   if (n == -1)
  //   {
  //     perror("Error receiving data");
  //     // Handle the error appropriately

  //     return 0;
  //   }
  //   else if (n == 0)
  //   {
  //     // Connection closed by the remote host
  //     printf("Connection closed by the remote host.\n");

  //     return 0;
  //   }
  //   // Data received successfully
  //   bzero(buffer, SIZE);
  // } while (n >= 0);

  n = recv(sockfd, buffer, SIZE, 0);
  ptr = buffer;
  printf("\n%s", ptr);

  fprintf(fp, "%d. %s", ++i, ptr);
  if (n == -1)
  {
    perror("Error receiving data");
    // Handle the error appropriately

    return 0;
  }
  else if (n == 0)
  {
    // Connection closed by the remote host
    printf("Connection closed by the remote host.\n");

    return 0;
  }
  // Data received successfully
  bzero(buffer, SIZE);

  fclose(fp);
  return 1;
}

int send_file(char *file_name, int sockfd)
{
  FILE *fp;
  fp = fopen(file_name, "r");
  if (fp == NULL)
  {
    perror("[-]Error in reading file.");
    return 0;
  }
  int n;
  char *ptr = NULL;
  char buffer[SIZE] = {0};
  size_t bytesRead;
  char data[MAX_DATA_LENGTH] = {0};
  sprintf(data, "upload %s", file_name);
  printf("Sending login data: %s\n", data);

  if (send(sockfd, data, strlen(data), 0) == -1)
  {
    perror("[-]Error in sending login data.");
    return 0;
  }
  sleep(1);

  n = recv(sockfd, buffer, SIZE, 0);
  ptr = buffer;
  printf("\n%s", ptr);

  if (strstr(buffer, "already exsits"))
  {
    perror(buffer);
    return 0;
  }
  bzero(buffer, SIZE);

  while ((bytesRead = fread(buffer, sizeof(char), SIZE, fp)) > 0)
  {

    if (send(sockfd, buffer, bytesRead, 0) == -1)
    {
      perror("[-]Error in sending file.");
      return 0;
    }
  }

  bzero(buffer, SIZE);
  fclose(fp);
  return 1;
}

int create_socket()
{
  int sockfd;
  struct sockaddr_in server_addr;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
  {
    perror("[-]Error in socket");
    exit(1);
  }
  printf("[+] Server socket created successfully.\n");

  server_addr.sin_family = AF_INET;
  server_addr.sin_port = PORT;
  server_addr.sin_addr.s_addr = inet_addr(IP);

  if (connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1)
  {
    perror("[-]Error in connecting to the server");
    exit(1);
  }
  printf("[+] Connected to the server.\n");

  return sockfd;
}

int login(int sockfd)
{

  char username[MAX_USERNAME_LENGTH] = {0};
  char password[MAX_PASSWORD_LENGTH] = {0};
  char data[MAX_DATA_LENGTH] = {0};
  printf("Log in please: \n");
  printf("Username: ");
  scanf("%s", username);

  printf("Password: ");
  scanf("%s", password);

  sprintf(data, "login , %s , %s ", username, password);

  // Perform authentication logic here
  // You can compare the username and password against stored credentials or a database

  printf("Sending login data: %s\n", data);

  if (send(sockfd, data, strlen(data), 0) == -1)
  {
    perror("[-]Error in sending login data.");
    exit(1);
  }
  char buffer[SIZE] = {0};

  int n = recv(sockfd, buffer, SIZE, 0);
  printf("buffer->\n%s", buffer);
  if (strcmp(buffer, "login failed!") == 0)
  {
    printf("\nLogin failed\n");
    return 0;
  }

  return 1;
}

// int sing_in(int sockfd)
// {
//   char username[MAX_USERNAME_LENGTH] = {0};
//   char password[MAX_PASSWORD_LENGTH] = {0};
//   char data[MAX_DATA_LENGTH] = {0};
//   printf("Sign in please: \n");
//   printf("Username: ");
//   scanf("%s", username);

//   printf("Password: ");
//   scanf("%s", password);

//   sprintf(data, "sign_in , %s , %s ", username, password);

//   // Perform authentication logic here
//   // You can compare the username and password against stored credentials or a database

//   printf("Sending sign in data: %s\n", data);

//   if (send(sockfd, data, strlen(data), 0) == -1)
//   {
//     perror("[-]Error in sending sign in.");
//     exit(1);
//   }
//   // char buffer[SIZE] = {0};

//   // int n = recv(sockfd, buffer, SIZE, 0);
//   // printf("buffer->\n%s", buffer);

//   return 1;
// }

void toUpperCase(char *ch)
{
  if (*ch >= 'a' && *ch <= 'z')
  {
    // Convert lowercase to uppercase by subtracting the difference between the ASCII values
    *ch = *ch - ('a' - 'A');
  }
}
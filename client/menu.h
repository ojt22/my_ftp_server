#if !defined(MENU)
#define MENU
#define FILENAME_LENGTH 300
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
void print_menu();
extern int send_file(char *file_name, int sockfd);
extern int create_socket();
extern void toUpperCase(char *ch);
extern int _download_file(char *file_name, int sockfd);
#endif // MENU

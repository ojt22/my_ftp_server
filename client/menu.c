#include "menu.h"
// extern int send_file(char *file_name, int sockfd);
// extern int create_socket();
void print_menu()
{
    char filename[FILENAME_LENGTH] = {0};
    int sockfd;
    char choose;
    do
    {
        sockfd = create_socket();
        printf("Please choose U (upload) / D (download) / Q (quit): ");
        scanf(" %c", &choose);
        toUpperCase(&choose);

        switch (choose)
        {
        case 'U':
            printf("Enter the filename to upload: ");
            scanf("%s", filename);
            if (send_file(filename, sockfd) == 0)
            {
                printf("[+] File data sent failed.\n");
                close(sockfd);
                break;
            }

            printf("[+] File data sent successfully.\n");
            close(sockfd);
            break;
        case 'D':

            printf("Enter the filename to download: ");
            scanf("%s", filename);
            if (_download_file(filename, sockfd) == 0)
            {
                printf("[+] File data dowload failed.\n");
                close(sockfd);
                break;
            }
            printf("[+] File data download successfully.\n");
            close(sockfd);
            break;
        case 'Q':
            printf("Quitting...\n");
            close(sockfd);
            break;
        default:
            printf("Invalid choice. Please try again.\n");
            close(sockfd);
            break;
        }

    } while (choose != 'Q');
}
#if !defined(LOGGER)
#define LOGGER
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

void log_message(const char *type, const char *message);


#endif // LOGGER

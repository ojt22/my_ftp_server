
#include "logger.h"
void log_message(const char *type, const char *message)
{
  FILE *file;
  time_t current_time;
  char *time_string;
  
  // Open the log file in append mode
  file = fopen("log.txt", "a");
  if (file == NULL)
  {
    perror("[-]Error in opening log file");
    exit(1);
  }

  // Get the current time
  current_time = time(NULL);
  time_string = ctime(&current_time);

  // Remove the newline character from the time string
  time_string[strcspn(time_string, "\n")] = '\0';

  // Print the log message to the file
  fprintf(file, "[%s] %s: %s\n", time_string, type, message);

  // Close the log file
  fclose(file);
}

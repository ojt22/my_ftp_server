#if !defined(THREADS)
#define THREADS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include "structchers.h"
#include "../logger/logger.h"
#include "../server/ftp_server.h"
// Define the maximum number of concurrent clients
#define MAX_CLIENTS 10
#define SIZE 1024

// Structure to hold client-specific data
extern int login(char *buffer, int new_sock);

void* handleClient(void* arg);


#endif // THREADS

#include "threads.h"

// Function to handle each client connection
void *handleClient(void *arg)
{   char *token=NULL;
    ClientData *clientData = (ClientData *)arg;
    int n;
    // Process client requests
    char buffer[SIZE];
    n = recv(clientData->socket, buffer, SIZE, 0);
    if (strlen(buffer) > 1)
        printf("\n%s\n", buffer);

    if (strstr(buffer, "login") != NULL)
    {
        if (login(buffer, clientData->socket) == 0)
        {

            log_message("INFO", "Client falied to log in.");
            printf("\nClient falied to log in\n");
            bzero(buffer, SIZE);
            close(clientData->socket);
            free(clientData);
            pthread_exit(NULL);
        }

        bzero(buffer, SIZE);
        printf("\nClient login\n");
        log_message("INFO", "Client login.");
        close(clientData->socket);
        free(clientData);
        pthread_exit(NULL);

       
    }
    else if (strstr(buffer, "upload") != NULL)
    {
        

        token = strtok(buffer, " ");
        token = strtok(NULL, " ");

        if (write_file(token, clientData->socket) == 0)
        {
            printf("[-]Error write file.");
            
        }
        else
        {

            printf("[+]Data written in the file successfully.\n");
            bzero(buffer, SIZE);
            log_message("INFO", "Client upload file.");
        }
    }
    else if (strstr(buffer, "download") != NULL)
    {
        token = strtok(buffer, " ");
        token = strtok(NULL, " ");
        printf("\nClient download file ,%s", token);
        if (send_file(token, clientData->socket) == 0)
        {
            printf("[-]Error write file.");
        }
        else
        {

            printf("[+]file send successfully.\n");
            bzero(buffer, SIZE);
            log_message("INFO", "Client download file.");
        }
    }

    // // Cleanup and close the client socket
    free(clientData);
    pthread_exit(NULL);
   
}

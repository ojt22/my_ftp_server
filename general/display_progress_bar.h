#if !defined(DISPLAY_PROGRESS_BAR)
#define DISPLAY_PROGRESS_BAR
#include <stdio.h>
#include<unistd.h>
void displayProgressBar(int progress, int total);

#endif // DISPLAY_PROGRESS_BAR

#include "delete_spaces.h"


void deleteSpaces(char* str) {
    if (str == NULL)
        return;

    char* dest = str;
    char* src = str;

    while (*src != '\0') {
        if (!isspace(*src)) {
            *dest = *src;
            dest++;
        }
        src++;
    }

    *dest = '\0';
}
#if !defined(DELETE_SPACES)
#define DELETE_SPACES
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


void deleteSpaces(char* str) ;
#endif // DELETE_SPACES

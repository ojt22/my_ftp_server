#if !defined(STRUCTCHERS)
#define STRUCTCHERS
// #include "../server/ftp_server.h"
#include <arpa/inet.h>
typedef struct ftp_server
{
    int sockfd;
    struct sockaddr_in server_addr, new_addr;
    socklen_t addr_size;

} ftp_server;
typedef struct
{
    int socket;
    ftp_server ftp_serverfd;
} ClientData;

#endif // STRUCTCHERS

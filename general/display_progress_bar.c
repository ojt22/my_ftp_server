#include "display_progress_bar.h"



void displayProgressBar(int progress, int total) {
    const int barWidth = 50;
    float percent = (float)progress / total;
    int filledWidth = (int)(percent * barWidth);

    printf("\n[");
    for (int i = 0; i < barWidth; i++) {
        if (i < filledWidth) {
            printf("=");
        } else {
            printf(" ");
        }
    }
    printf("] %.2f%%\r", percent * 100);

    // Flush the output to ensure it's displayed immediately
     fflush(stdout);
}
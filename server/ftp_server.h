#if !defined(FTP_SERVER)
#define FTP_SERVER
#include <stdio.h>
#include <stdlib.h>
#include<stdbool.h>
#include <string.h>
// #include <arpa/inet.h>
#include "../logger/logger.h"
#include "../users/user.h"
#include "../general/delete_spaces.h"
#include "../general/threads.h"
#include "../general/structchers.h"
#include "../general/generate_id.h"
#define SIZE 1024
#define IP "127.0.0.1"
#define PORT 8088
#define FILENAME_LENGTH 300



ftp_server* creat_socket();
int write_file(char * file_name,int sockfd);
void send_response(int sockfd, const char *response);
int send_file(char *file_name, int sockfd);
int login(char* buffer,int new_sock);
int sing_in(int new_sock, char *user_details);
bool is_file_exsit(char *str);
void creat_list_of_users();
#endif // FTP_SERVER

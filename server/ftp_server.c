#include "ftp_server.h"
// gcc -o ftp_ser ftp_server.c ../logger/logger.c ../users/user.c ../general/delete_spaces.c ../general/threads.c -lpthread ../general/structchers.h
// git push https://gitlab.com/ojt22/my_ftp_server.git
User *userList = NULL;

int main()
{

    // creat list of registers users for the client log in

    creat_list_of_users();
    int i = 0;

    char buffer[SIZE];

    ftp_server *ftp_serverfd = creat_socket();

    int n, new_sock;
    pthread_t clientThreads[MAX_CLIENTS];
    int numClients = 0;
    char *token = NULL;
    while (1)
    {
        ftp_serverfd->addr_size = sizeof(ftp_serverfd->new_addr);
        new_sock = accept(ftp_serverfd->sockfd, (struct sockaddr *)&ftp_serverfd->new_addr, &ftp_serverfd->addr_size);

        if (numClients >= MAX_CLIENTS)
        {
            printf("Maximum number of clients reached. Connection rejected.\n");
            close(new_sock);
            numClients=0;
            continue;
        }

        // Create client data structure
        ClientData *clientData = (ClientData *)malloc(sizeof(ClientData));
        clientData->socket = new_sock;
        // Create thread to handle client
        pthread_create(&clientThreads[numClients], NULL, handleClient, (void *)clientData);
        numClients++;
    }

    return 0;
}

void creat_list_of_users()
{
    long rand_id = generateUniqueID();
    User *user1 = createUser(rand_id++, "john_doe", "1234");
    User *user2 = createUser(rand_id++, "jane_smith", "2666");
    User *user3 = createUser(rand_id++, "bob_jones", "6549");

    insertUser(&userList, user1);
    insertUser(&userList, user2);
    insertUser(&userList, user3);
    // Printing the user list
    printUsers(userList);
}

bool is_file_exsit(char *file_name)
{
    FILE *fp;
    fp = fopen(file_name, "r");
    return fp != NULL;
}

ftp_server *creat_socket()
{
    ftp_server *ftp = (ftp_server *)malloc(sizeof(ftp_server));
    int e;
    int sockfd, new_sock;
    struct sockaddr_in server_addr, new_addr;
    socklen_t addr_size;
    ftp->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (ftp->sockfd < 0)
    {
        perror("[-]Error in socket");
        exit(1);
    }
    printf("[+]Server socket created successfully.\n");
    ftp->server_addr.sin_family = AF_INET;
    ftp->server_addr.sin_port = PORT;
    ftp->server_addr.sin_addr.s_addr = inet_addr(IP);

    e = bind(ftp->sockfd, (struct sockaddr *)&ftp->server_addr, sizeof(ftp->server_addr));

    if (e < 0)
    {
        perror("[-]Error in bind");
        exit(1);
    }
    printf("[+]Binding successfull.\n");

    if (listen(ftp->sockfd, 10) == 0)
    {
        printf("[+]Listening....\n");
    }
    else
    {
        perror("[-]Error in listening");
        exit(1);
    }
    return ftp;
}
int write_file(char *file_name, int sockfd)
{
    int n;
    FILE *fp;
    char filename[FILENAME_LENGTH] = {0};
    char buffer[SIZE] = {0};

    sprintf(filename, "recive_%s", file_name);
    if (is_file_exsit(filename))
    {

        send_response(sockfd, "File already exsits");
        printf("Connection closed by the remote host.\n");
        log_message("INFO", "[-]File already exsits");

        return 0;
    }
    send_response(sockfd, "write file");
    fp = fopen(filename, "w");
    if (fp == NULL)
    {
        const char ERROR[] = "[-]Error in writing file.";
        send_response(sockfd, ERROR);
        printf(ERROR);
        log_message("ERROR", ERROR);
        return 0;
    }

    char *ptr = NULL;
    int i = 0;
    while (1)
    {
        n = recv(sockfd, buffer, SIZE, 0);
        ptr = buffer;
        //printf("\n%s", ptr);
        fprintf(fp, "%d. %s", ++i, ptr);
        if (n == -1)
        {
            perror("Error receiving data");
            // Handle the error appropriately
            log_message("ERROR", "[-]Error receiving data.");
            fclose(fp);
            return 0;
        }
        else if (n == 0)
        {
            // Connection closed by the remote host
            printf("Connection closed by the remote host.\n");
            log_message("INFO", "[-]Connection closed by the remote host.");
            fclose(fp);
            return 1;
        }

        // Data received successfully
        bzero(buffer, SIZE);
    }
    fclose(fp);
    return 1;
}
void send_response(int sockfd, const char *response)
{
    printf("sending response %s\n\n", response);
    if (send(sockfd, response, strlen(response), 0) == -1)
    {
        perror("[-]Error in sending response.");
        log_message("ERROR", "[-]Error in sending response.");
        exit(1);
    }
}
int send_file(char *file_name, int sockfd)
{
    char buffer[SIZE] = {0};
    FILE *fp;
    size_t bytesRead;
    fp = fopen(file_name, "r");
    if (fp == NULL)
    {
        char ERROR[] = "[-]Error in reading file .";
        perror(ERROR);
        log_message("ERROR", ERROR);
        if (send(sockfd, ERROR, sizeof(ERROR), 0) == -1)
        {
            perror("[-]Error in sending file.");
            log_message("ERROR", "[-]Error in sending file ");
        }
        return 0;
    }

    while ((bytesRead = fread(buffer, sizeof(char), SIZE, fp)) > 0)
    {
        printf("%s\n\n", buffer);
        if (send(sockfd, buffer, bytesRead, 0) == -1)
        {
            perror("[-]Error in sending file.");
            log_message("ERROR", "[-]Error in sending file ");
            exit(1);
        }
    }

    fclose(fp);
    return 1;
}

int login(char *buffer, int new_sock)
{

    char first_name[50] = {0};
    char password[50] = {0};
    char *token = strtok((char *)buffer, ",");
    token = strtok(NULL, ",");

    strcpy(first_name, token);
    deleteSpaces(first_name);
    token = strtok(NULL, ",");
    strcpy(password, token);
    deleteSpaces(password);
    User *user = createUser(time(NULL), first_name, password);

    if (isUserExists(userList, user))
    {
        send_response(new_sock, "login sucssesfully");
        printf("login sucssesfully!\n");
        return 1;
    }
    printf("\nlogin failed!\n");
    send_response(new_sock, "login failed!");
    return 0;
}
int sing_in(int new_sock, char *user_details)
{

    enterUserDetails(&userList, user_details);

    printUsers(userList);
    send_response(new_sock, "Sign in sucssesfully");
    printf("Sing in sucssesfully Welcome!\n");
    return 1;
}
#if !defined(USER)
#define USER
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>
typedef struct User
{
    long id;
    struct User *next;
    char user_name[50];
    char password[50];
} User;
User *createUser(long id, const char *user_name, const char *password);
void insertUser(User** head, User* newUser);
void printUsers(User* head) ;
void freeUsers(User* head);
void enterUserDetails(User** head, const char* userDetails);
bool isUserExists(User* head, User* targetUser);
#endif // USER

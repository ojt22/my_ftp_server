#include "user.h"

User *createUser(long id, const char *user_name, const char *password)
{
    User *newUser = (User *)malloc(sizeof(User));
    if (newUser == NULL)
    {
        fprintf(stderr, "Memory allocation failed.\n");
        return NULL;
    }
    newUser->id = id;
    newUser->next = NULL;
    strncpy(newUser->user_name, user_name, sizeof(newUser->user_name) - 1);
    strncpy(newUser->password, password, sizeof(newUser->password) - 1);
    return newUser;
}

void insertUser(User **head, User *newUser)
{
    if (*head == NULL)
    {
        *head = newUser;
    }
    else
    {
        User *current = *head;
        while (current->next != NULL)
        {
            current = current->next;
        }
        current->next = newUser;
    }
}

void printUsers(User *head)
{
    printf("User List:\n");
    User *current = head;
    while (current != NULL)
    {
        printf("ID: %ld, User Name: %s, Password: %s\n", current->id, current->user_name, current->password);
        current = current->next;
    }
}

void freeUsers(User *head)
{
    User *current = head;
    while (current != NULL)
    {
        User *temp = current;
        current = current->next;
        free(temp);
    }
}
void enterUserDetails(User **head, const char *userDetails)
{
    char *token = strtok((char *)userDetails, ",");
    token = strtok(NULL, ",");
    if (token == NULL)
    {
        fprintf(stderr, "Invalid user details format.\n");
        return;
    }
    time_t id = time(NULL);
    char user_name[50];
    char password[50];

    strncpy(user_name, token, sizeof(user_name) - 1);

    token = strtok(NULL, ",");
    if (token == NULL)
    {
        fprintf(stderr, "Invalid user details format.\n");
        return;
    }
    strncpy(password, token, sizeof(password) - 1);

    User *newUser = createUser(id, user_name, password);
    insertUser(head, newUser);
}
bool isUserExists(User *head, User *targetUser)
{
   printf("ID: %ld, User Name: %s, Password: %s\n", targetUser->id, targetUser->user_name, targetUser->password);
    User *current = head;
    while (current != NULL)
    {
        if (strstr(current->user_name, targetUser->user_name) && strstr(current->password, targetUser->password) )
        {
            
            //  printf("ID: %ld, User Name: %s, Password: %s\n", current->id, current->user_name, current->password);
            return true;
        }
        current = current->next;
    }
    return false;
}
// int main() {
//     User* userList = NULL;

//     // Creating users
//     User* user1 = createUser(time(NULL), "john_doe", "123456");
//     User* user2 = createUser(time(NULL), "jane_smith", "qwerty");
//     User* user3 = createUser(time(NULL), "bob_jones", "password");

//     // Inserting users into the list
//     insertUser(&userList, user1);
//     insertUser(&userList, user2);
//     insertUser(&userList, user3);

//     // Printing the user list
//     printUsers(userList);

//     // Freeing the user list
//     freeUsers(userList);

//     return 0;
// }
